bikes <- read.csv('train_bike.csv', sep = ",", header = T)

str(bikes)

bikes$datetime <- as.character(bikes$datetime)

bikes$date <- sapply(strsplit(bikes$datetime, ' '), "[", 1) #first part of the split

bikes$date <- as.Date(bikes$date)

bikes$time <- sapply(strsplit(bikes$datetime, ' '), "[", 2)

bikes$hour <- sapply(strsplit(bikes$time, ':'), "[", 1)

bikes$hour <- as.numeric(bikes$hour)

#is na?
any(is.na(bikes))

#create numeric vector
col.numeric<- sapply(bikes, is.numeric)

#filter by numeric vector
bikes_numeric<-bikes[,col.numeric]

#view of the subset of the data set
bikes_numeric

#correlations
cor_data<-cor(bikes_numeric)

#show correlation chart
install.packages("corrplot")
library(corrplot)

print(corrplot(cor_data, method = 'color'))
plot(bikes$casual,bikes$count)
plot(bikes$temp,bikes$count)

hist(bikes$count, col = 'red')

#split the data into train and test
install.packages("caTools")
library(caTools)

set.seed(101)

sample<-sample.split(bikes$count, SplitRatio = 0.7)

train<-subset(bikes, sample)
test<-subset(bikes, !sample)

nrow(bikes)
nrow(train)
nrow(test)
nrow(test)+nrow(train)

model<-lm(count ~ season+holiday+workingday+weather+temp+humidity+windspeed+casual+registered,data=train)
print(summary(model))

#data is not normalaized
res <- residuals(model)
hist(res)

#predictions on test set
count_predict<-predict(model,test)

# computing error on test train
results<- cbind(count_predict, test$count)
colnames(results) <- c('Predicted', 'Actual')
results<- as.data.frame(results)
min(results)

#no need to cut negative
#cutting negative values
to_zero <-function(x) {
  
      if(x<0){
        return (0)
      } else {
        return (x)
      }
}


#mean square errors
MSE <- mean((results$Actual-results$Predicted)^2)
RMSE <- MSE^0.5

#computing overfitting effect
count_predict_train <- predict(model,train)
results_train <- cbind(count_predict_train, train$count)
colnames(results_train)<- c('Predicted','Actual')
results_train<- as.data.frame(results_train)
results_train$Predicted <- sapply(results_train$Predicted ,to_zero)

MSE_train <- mean((results_train$Actual-(results_train$Predicted)^2))
RMSE_train <- MSE_train^0.5


