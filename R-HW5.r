#Load data
adult <- read.csv(url('https://raw.githubusercontent.com/Geoyi/Salary-prediction/master/adult_sal.csv'))
#define countrries level
Europe <- c("England","Greece", "Hungary", "Ireland", "Holand-Netherlands",
            "Poland" , "Italy","Scotland", "Yugoslavia", "Germany" , "France",
            "Portugal")
Asia <- c("China" ,"Japan" , "Hong", "Cambodia", "Philippines",  "Iran", 
          "Taiwan", "Thailand" ,  "Laos", "India",  "Vietnam"    )
North.america <- c("United-States" ,  "Canada", "Puerto-Rico"  ) 
Latin.and.south.america <- c("Columbia", "Cuba", "Dominican-Republic",
                             "Ecuador", "El-Salvador",  "Guatemala", "Haiti" ,
                             "Honduras", "Jamaica", "Nicaragua", "Mexico","Peru",
                             "Outlying-US(Guam-USVI-etc)" ,"Trinadad&Tobago" )
Other <- c("South")

#replace country with level
adult$mainland <- ""
adult[(adult$country %in%  Europe),]$mainland <-"Europe"
adult[(adult$country %in%  Asia),]$mainland <-"Asia"
adult[(adult$country %in%  North.america),]$mainland <-"North.america"
adult[(adult$country %in%  Latin.and.south.america),]$mainland <-"Latin.and.south.america"
adult[(adult$country %in%  Other),]$mainland <-"Other"

unique(adult$marital)
#New levels: Never-Married, Married, Was-Married
Never.married<-c("Never-married")
Married<-c("Married-civ-spouse","Married-spouse-absent", "Married-AF-spouse")
Was.married<-c("Separated", "Widowed", "Divorced")


unique(adult$type_employer)
#New levels: self-emp, never-worked, private, gov, without-pay
Self.emp<-c("Self-emp-inc","Self-emp-not-inc")
Never.worked<-c("Never-worked")
Private<-c("Private")
Gov<-c("State-gov","Self-emp-not-inc Private","Federal-gov","Local-gov")
Without.pay<-c("Without-pay")

#change marital to levels
adult$marital_levels <- ""
adult[(adult$marital %in%  Never.married),]$marital_levels <-"Never.married"
adult[(adult$marital %in%  Married),]$marital_levels <-"Married"
adult[(adult$marital %in%  Was.married),]$marital_levels <-"Was.married"

#change type_employer to levels
adult$type_employer_levels <- ""
adult[(adult$type_employer %in%  Self.emp),]$type_employer_levels <-"Self.emp"
adult[(adult$type_employer %in%  Never.worked),]$type_employer_levels <-"Never.worked"
adult[(adult$type_employer %in%  Private),]$type_employer_levels <-"Private"
adult[(adult$type_employer %in%  Gov),]$type_employer_levels <-"Gov"
adult[(adult$type_employer %in%  Without.pay),]$type_employer_levels <-"Without.pay"

#remove missing data
adult[adult == '?'] <- NA
adult_clear<-adult[complete.cases(adult), ]

#remove index column
adult_clear$X<-NULL

#histogram to review the effect of the variables on the target attribute
library(ggplot2)
ggplot(adult_clear, aes(income)) + geom_bar()
ggplot(adult_clear, aes(income, fill = factor(type_employer_levels)))+ geom_bar()
ggplot(adult_clear, aes(income, fill = factor(marital_levels)))+ geom_bar()
ggplot(adult_clear, aes(income, fill = factor(mainland)))+ geom_bar()

#turn into factors
adult_clear$income <- factor(adult_clear$income)
adult_clear$mainland<- factor(adult_clear$mainland)
adult_clear$marital_levels<- factor(adult_clear$marital_levels)
adult_clear$type_employer_levels<- factor(adult_clear$type_employer_levels)

#keep only the values columns
adult_clear<-adult_clear[,c(1,3,4,5,7:13,15:18)]

#dividing into test set and training set 
library(dplyr)
adult_clear_train<-sample_frac(adult_clear, 0.7)

#row numbers in the trai set 
sid<-as.numeric(rownames(adult_clear_train)) # because rownames() returns character
adult_clear_test<-adult_clear[-sid,]

#The logistic regression model 
log.model <- glm(income ~ . , family = binomial(link = logit), data = adult_clear_train)

summary(log.model)

predicted.probabilities <- predict(log.model,adult_clear_test, type = 'response')
predicted.values <- ifelse(predicted.probabilities>0.5,1,0)

misClassError <- mean(predicted.values != adult_clear_test$income)

#Confusion matrix
cm <- table(adult_clear_test$income,predicted.probabilities>0.5)