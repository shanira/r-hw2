#read data
train_titanic<- read.csv("titanic_train.csv")

sum(is.na(train_titanic$Age))

#replace age missing values with mean
train_titanic$Age[which(is.na(train_titanic$Age))]<-mean(train_titanic$Age, na.rm = TRUE)
sum(is.na(train_titanic$Age))

#change into factor the Survived column
train_titanic$Survived<-as.factor(train_titanic$Survived)
levels(train_titanic$Survived)<- c('No', 'Yes')

#Histograms of age, fare and sibSp
install.packages("ggplot2")
library(ggplot2)
p_age <- ggplot(train_titanic, aes(x = Age))
p_age <- p_age + geom_bar(aes(fill = Survived))
p_age

p_fare <- ggplot(train_titanic, aes(x = Fare))
p_fare <- p_fare + geom_bar(aes(fill = Survived))
p_fare

p_sib <- ggplot(train_titanic, aes(x = SibSp))
p_sib <- p_sib + geom_bar(aes(fill = Survived))
p_sib

survived.equation<-"Survived ~ Pclass + Sex +Age + SibSp + Parch + Fare + Embarked"
survived.formula<-as.formula(survived.equation)
