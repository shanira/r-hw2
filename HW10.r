#load the libreries
library(ggplot2)
library(cluster)
#read the data
red<-read.csv("winery_1.csv", sep = ";")
white<-read.csv("winery_2.csv", sep = ";")
#add label type
red$type<-"red"
white$type<-"white"
#comnbine 2 dataframes
wine<-rbind(red, white)
#Show a histogram of residual.sugar 
p<-ggplot(data=wine, aes(residual.sugar))+ geom_bar(aes(fill= type), alpha=5/10)
plot(p)
#conclusion: there is more white wine with residual suger

#scatter plots to seek for a separating factor 
s1<- ggplot(data=wine, aes(chlorides, fixed.acidity, color=type)) +geom_point()
plot(s1)

s2<-s1<- ggplot(data=wine, aes(wine$volatile.acidity, wine$pH, color=type)) +geom_point()
plot(s2)

s3<- ggplot(data=wine, aes(wine$alcohol,wine$sulphates, color=type)) +geom_point(alpha=5/10)
plot(s3)

#Kmean clustering model
wine_clus<-wine
wine_clus$type<-NULL

set.seed(101)
results<-kmeans(wine_clus,2)
table(wine$type,results$cluster)
#the results of the algorithen is not so good.
clusplot(wine,diss=T,results$cluster, color=T,shade = T,labels = 0, lines = 0)
