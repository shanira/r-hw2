library(lubridate)
library(scales)
library(plyr)
library(readr)

bike <- read.csv("train.csv")
str(bike)
bike$datetime <- as.character(bike$datetime)
bike$date <- sapply(strsplit(bike$datetime,' '), "[", 1)

bike$date <- as.Date(bike$date)
bike$time <- sapply(strsplit(bike$datetime,' '), "[", 2)
bike$hour <-  sapply(strsplit(bike$time,':'), "[", 1)
bike$hour <- as.numeric(bike$hour)

bike$times  <- as.POSIXct(strftime(ymd_hms(bike$datetime), format="%H:%M:%S"), format="%H:%M:%S")

bike$hour    <- factor(hour(ymd_hms(bike$datetime)))

#-------------Q1

Q1<- ggplot(bike, aes(x= count, y=temp))
Q1 + geom_point()+ ggtitle("how temperature affects the count")

#-------------Q2

Q2<- ggplot(bike, aes(x= date, y=count))
Q2 + geom_point()+ ggtitle("how date affects the count")

#-------------Q3

bike$season  <- factor(bike$season, labels = c("Spring", "Summer", "Fall", "Winter"))
Q3 <- ggplot(bike, aes(x=season, y=count)) + geom_boxplot()+ ggtitle("boxplot count and season")
Q3

#------------Q4

a<- ddply(bike,.(temp,hour), summarise, count = mean(count))

Q4<-ggplot(bike, aes(x = hour, y = count, colour = temp)) +
  geom_point(data = a, aes(group = temp)) +
  geom_line(data = a, aes(group = temp)) +
  scale_x_discrete("Hour") +
  scale_y_continuous("Count") +
  theme_minimal() +
  ggtitle("How hour and temperature together affects count") + 
  theme(plot.title=element_text(size=18))

Q4

#create weekday column
bike$Weekday <- wday(ymd_hms(bike$datetime), label=TRUE)

#split to two df by sunday and weekday
bike$sunday_vs_weekdays<- ifelse(bike$Weekday == 'Sun', 'Sunday', 'weekdays')
bike_weekdays<-bike[bike$Weekday != 'Sun',]
bike_sunday<-bike[bike$Weekday == 'Sun',]

data_sunday<- ddply(bike_sunday,.(temp,hour), summarise, count = mean(count))


Q4_sunday<-ggplot(bike_sunday, aes(x = hour, y = count, colour = temp)) +
  geom_point(data = data_sunday, aes(group = temp)) +
  geom_line(data = data_sunday, aes(group = temp)) +
  scale_x_discrete("Hour") +
  scale_y_continuous("Count") +
  theme_minimal() +
  ggtitle("sunday temp and hour") + 
  theme(plot.title=element_text(size=18))

Q4_sunday

data_weekdays<- ddply(bike_weekdays,.(temp,hour), summarise, count = mean(count))

Q4_weekdays<-ggplot(bike_weekdays, aes(x = hour, y = count, colour = temp)) +
  geom_point(data = data_weekdays, aes(group = temp)) +
  geom_line(data = data_weekdays, aes(group = temp)) +
  scale_x_discrete("Hour") +
  scale_y_continuous("Count") +
  theme_minimal() +
  ggtitle("sunday temp and hour") + 
  theme(plot.title=element_text(size=18))

Q4_weekdays

#function to multiplot charts
multiplot <- function(..., plotlist=NULL, file, cols=1, layout=NULL) {
  require(grid)
  
  # Make a list from the ... arguments and plotlist
  plots <- c(list(...), plotlist)
  
  numPlots = length(plots)
  
  # If layout is NULL, then use 'cols' to determine layout
  if (is.null(layout)) {
    # Make the panel
    # ncol: Number of columns of plots
    # nrow: Number of rows needed, calculated from # of cols
    layout <- matrix(seq(1, cols * ceiling(numPlots/cols)),
                     ncol = cols, nrow = ceiling(numPlots/cols))
  }
  
  if (numPlots==1) {
    print(plots[[1]])
    
  } else {
    # Set up the page
    grid.newpage()
    pushViewport(viewport(layout = grid.layout(nrow(layout), ncol(layout))))
    
    # Make each plot, in the correct location
    for (i in 1:numPlots) {
      # Get the i,j matrix positions of the regions that contain this subplot
      matchidx <- as.data.frame(which(layout == i, arr.ind = TRUE))
      
      print(plots[[i]], vp = viewport(layout.pos.row = matchidx$row,
                                      layout.pos.col = matchidx$col))
    }
  }
}

multiplot(Q4_weekdays,Q4_sunday, cols = 2 )


#----------Q5
bike$weather <- factor(bike$weather, labels = c("Good", "Normal", "Bad", "Very Bad"))

data_weather <- ddply(bike,.(weather,hour),
                      summarise, count = mean(count))

ggplot(bike, aes(x = hour, y = count, colour = weather)) +
  geom_point(data = data_weather, aes(group = weather)) +
  geom_line(data = data_weather, aes(group = weather)) +
  scale_x_discrete("Hour") +
  scale_y_continuous("Count") +
  theme_minimal() +
  ggtitle("People rent bikes more when the weather is Good.\n") + 
  theme(plot.title=element_text(size=18))
